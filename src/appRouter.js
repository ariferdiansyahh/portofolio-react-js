import React, { Fragment } from "react"
import { BrowserRouter, Route, Link } from "react-router-dom"
import './styles.css'
import { slide as Menuu } from "react-burger-menu";
import { Menu, Icon } from 'semantic-ui-react'

import Latihan from './components/Latihan/App'
import Home from './components/page/Home/home'
import User from './components/page/User/user'
import Web from './components/page/Website/web'
import Notif from './components/page/Notification/notif'
import Login from './components/page/Login/Login'
import Usertrans from './components/page/User_transaction/user_transaction'
import Costumer from './components/page/Costumer_transaction/constumer_transaction'


const AppRouter = (props) => (
    <BrowserRouter>
        <Fragment >

            <Menuu {...props} >
                <Menu inverted style={{ backgroundColor: "Black", width: 270, outlineStyle:"none" }}>

                    <Menu.Item>
                        <h2>Dashboard </h2>

                    </Menu.Item>
                    <hr color="white"></hr>
                    <Menu.Item  >
                        <h4 ><Icon name='home' /> <Link to="/" style={{ color: "white" }}>Home</Link> </h4>
                    </Menu.Item>

                    <Menu.Item>
                        <h4><Icon name='user' /> <Link to="/user" style={{ color: "white" }}> User</Link></h4>
                    </Menu.Item>

                    <Menu.Item >
                        <h4><Icon name='world' /> <Link to="/web" style={{ color: "white" }}>Website</Link></h4>
                    </Menu.Item>

                    <Menu.Item >
                        <h4><Icon name='user circle ' /> <Link to="/user_transaction" style={{ color: "white" }}>User Transaction</Link></h4>
                    </Menu.Item>

                    <Menu.Item >
                        <h4><Icon name='usd' /> <Link to="/costumer_transaction" style={{ color: "white", }}>Costumer Transaction</Link></h4>
                    </Menu.Item>

                    <Menu.Item >
                        <h4>  <Icon name='bell' /> <Link to="/notif" style={{ color: "white" }}>Notification</Link></h4>
                    </Menu.Item>

                </Menu >
            </Menuu>



            <Route path="/" exact component={Home} />
            <Route path="/user" component={User} />
            <Route path="/web" component={Web} />
            <Route path="/user_transaction" component={Usertrans} />
            <Route path="/costumer_transaction" component={Costumer} />
            <Route path="/notif" component={Notif} />
            <Route path="/login" component={Login} />
            <Route path="/latihan" component={Latihan} />

        </Fragment>
    </BrowserRouter>
)
export default AppRouter