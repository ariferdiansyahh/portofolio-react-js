import React, { Component } from 'react'

import { Menu, Button } from 'semantic-ui-react'
import { Link } from "react-router-dom"
import logo from './img/logo2.png'
import Notif from './page/Notif/Notif'
import { instanceOf } from 'prop-types';
import { Cookies } from 'react-cookie';


export default class MenuExampleInvertedSegment extends Component {
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };
    onClicklogout(e) {
        console.log('logout')
        const cookies = new Cookies();
        cookies.remove('admin');
        window.location.href = '/login';
    }
    render() {
        const cookies = new Cookies();
        var session = cookies.get('admin');
        if (!session) {
            alert('Silahkan Login')
            window.location.href = '/login'
        }
        return (

            <Menu inverted style={{ height: 60 }}>

                <Menu.Menu position='left' style={{ paddingLeft: 80, paddingTop: 15 }}>
                    <Link to="/">

                        <img
                            src={logo}
                            alt="a"
                            height="30"
                        />

                    </Link>
                </Menu.Menu>

                <Menu.Menu position='right'>

                    <Menu.Item >
                        <Notif />
                    </Menu.Item>

                    <Menu.Item >

                        <Button basic inverted size='small' onClick={this.onClicklogout} >
                            LOGOUT
                            </Button>
                    </Menu.Item>

                </Menu.Menu>

            </Menu>

        )
    }
}