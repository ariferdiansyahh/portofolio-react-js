import React, { Component } from 'react';
import Header from '../../Header'
import "semantic-ui-css/semantic.min.css";
import { Modal } from 'react-bootstrap';
import {  Button } from 'semantic-ui-react'
import MUIDataTable from "mui-datatables";
import { Container, Segment } from 'semantic-ui-react'
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles'
import { Grid } from 'semantic-ui-react'
import Axios from 'axios'

export default class App extends Component {
  constructor(props) {
    super(props);

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.state = {
      data: [],
      data_transaction:[],
      transaction:[],
      user:[],
      name: '',
      email: '',
      show: false,
    }
  }
  componentDidMount = async () => {
    const res = await Axios.get(`http://localhost:8000/user-transactions`)
    const data = await res.data.data
    console.log(data)
    this.setState({
      data: data
    })
  }

  getMuiTheme = () => createMuiTheme({
    overrides: {
      MUIDataTableBodyCell: {
        root: {
          fontSize: 15
        }
      },
      MUIDataTableHeadCell: {
        root: {
          fontSize: 16
        }
      }
    }
  })

  onClickEdit(e) {
    console.log('Edit')
  }
  onClickAction(e) {
    console.log('Action')
  }

  handleClose() {
    this.setState({
      data_transaction:'',
      transaction:'',
      user:'',
      show: false
    });
  }

  handleShow = async (e) => {
    const obj =  e.target.getAttribute('uid')
    const data_uid = await obj 
    console.log(data_uid)
    if (data_uid !== null) {
      const res = await Axios.get('http://localhost:8000/user-transactions/' + data_uid)
      const data = await res.data.data
      const user = await res.data.data.user
      const transaction = await res.data.data.transaction
      this.setState({
        user: user,
        transaction: transaction,
        data_transaction : data,
        show:true
      })
    }


  }

  render() {
    const columns = [
      {
        name: "id",
        label: "ID",
        options: {
          filter: true,
          sort: false
        }
      }
      , {
        name: "user",
        label: "Name",
        options: {
          filter: false,
          sort: false,
          customBodyRender: (value, tableMeta, updateValue) => {
            if (tableMeta.rowData[3] !== undefined) {
              

              if (tableMeta.rowData[3]) {
                return tableMeta.rowData[3].name
              }
            }
          }
        }
      },
      {
        name: "transaction_id",
        label: "ID Merchant",
        options: {
          filter: false,
          sort: false,
        }
      },
      {
        name: "user",
        label: "Email",
        options: {
          filter: false,
          sort: false,
          customBodyRender: (value, tableMeta, updateValue) => {
            if (tableMeta.rowData[3] !== undefined) {

              if (tableMeta.rowData[3]) {
                return tableMeta.rowData[3].email
              }
            }
          }
        }
      },
      {
        name: "transaction_name",
        label: "Transaction Name",
        options: {
          filter: false,
          sort: false,
        }
      },
      {
        name: "Action",
        options: {
          filter: false,
          sort: false,
          customBodyRender: (value, tableMeta, updateValue) => {
            if (tableMeta.rowData !== undefined) {
              // console.log(tableMeta.rowData)
            }
            const data = tableMeta.rowData;
            if (data !== undefined) {
              if (data[0]) {
                return (
                  <div>
                    <Button.Group>
                      <Button primary uid={data[0]} content="Detail" onClick={this.handleShow} icon="clipboard outline"></Button>
                    </Button.Group>
                  </div>
                )
              }

            }


          }
        }
      },

    ];
    const data = this.state.data;

    const options = {
      filterType: 'dropdown',
      responsive: 'scroll',
      selectableRows: false
    };
    return (
      <div>
        <Header />
        <Segment basic style={{ textAlign: "center", zIndex: 0 }}>
          <h2>User Transaction</h2>
          <Container>
            < MuiThemeProvider theme={this.getMuiTheme()}>
              <MUIDataTable
                title={"LIST USER TRANSACTION"}
                data={data}
                columns={columns}
                options={options}
              />
            </MuiThemeProvider>
          </Container>
        </Segment>
        <Modal show={this.state.show} onHide={this.handleClose} size='lg' centered >
          <Modal.Header closeButton >
            <Modal.Title > Detail Transaction User</Modal.Title>
          </Modal.Header>
          <Modal.Body style={{width: '250px !important'}}> 
            <Container>
              <Grid divided='vertically'>
                <Grid.Row columns={2}>
                  <Grid.Column width={5}>
                    <p> Name</p>
                  </Grid.Column>
                  <Grid.Column width={7}>
                    <h6> : &nbsp; {this.state.user.name}</h6>
                  </Grid.Column>
                  <Grid.Column width={5}>
                    <p> Email</p>
                  </Grid.Column>
                  <Grid.Column width={7}>
                    <h6> : &nbsp; {this.state.user.email}</h6>
                  </Grid.Column>
                  <Grid.Column width={5}>
                    <p> Transaction ID Merchant</p>
                  </Grid.Column>
                  <Grid.Column width={7}>
                    <h6> : &nbsp; {this.state.data_transaction.transaction_id}</h6>
                  </Grid.Column>
                  <Grid.Column width={5}>
                    <p> Transaction Name</p>
                  </Grid.Column>
                  <Grid.Column width={7}>
                    <h6> : &nbsp; {this.state.data_transaction.transaction_name}</h6>
                  </Grid.Column>
                  <Grid.Column width={5}>
                    <p> Payment Date Time</p>
                  </Grid.Column>
                  <Grid.Column width={7}>
                    <h6> : &nbsp; {this.state.transaction.payment_date_time}</h6>
                  </Grid.Column>
                  <Grid.Column width={5}>
                    <p> Subtotal</p>
                  </Grid.Column>
                  <Grid.Column width={7}>
                    <h6> : &nbsp; {this.state.data_transaction.subtotal}</h6>
                  </Grid.Column>
                  <Grid.Column width={5}>
                    <p> transaction Status</p>
                  </Grid.Column>
                  <Grid.Column width={7}>
                    <h6> : &nbsp; {this.state.transaction.trxstatus}</h6>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Container>         
          </Modal.Body>
        </Modal>
      </div >
    )
  }
}