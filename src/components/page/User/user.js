import React, { Component } from 'react';
import Header from '../../Header'
import "semantic-ui-css/semantic.min.css";
import { Modal } from 'react-bootstrap';
import { Icon, Button } from 'semantic-ui-react'
import MUIDataTable from "mui-datatables";
import { Container, Segment } from 'semantic-ui-react'
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles'
import Axios from 'axios'
import { Form } from 'semantic-ui-react'

export default class App extends Component {
  constructor(props) {
    super(props);

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleStatus = this.handleStatus.bind(this);
    this.onChangename = this.onChangename.bind(this);
    this.onChangeemail = this.onChangeemail.bind(this);

    this.state = {
      data: [],
      name: '',
      email: '',
      show: false,
    }
  }
  componentDidMount = async () => {
    const res = await Axios.get(`http://localhost:8000/users`)
    const data = await res.data.data
    this.setState({
      data: data
    })
  }

  getMuiTheme = () => createMuiTheme({
    overrides: {
      MUIDataTableBodyCell: {
        root: {
          fontSize: 15
        }
      },
      MUIDataTableHeadCell: {
        root: {
          fontSize: 16
        }
      }
    }
  })

  onClickEdit(e) {
    console.log('Edit')
  }
  onClickAction(e) {
    console.log('Action')
  }

  handleClose() {
    this.setState({
      id: '',
      name: '',
      email: '',
      show: false
    });
  }

  handleShow = async (e) => {

    let data_uid = await e.target.getAttribute('uid')
    console.log(data_uid)
    if (data_uid !== null) {
      const res = await Axios.get('http://localhost:8000/users/' + data_uid)
      const data = await res.data.data
      this.setState({
        id: data_uid,
        name: data.name,
        email: data.email,
        show: true
      })
    }


  }
  onChangename(e) {
    this.setState({
      name: e.target.value
    });
  }
  onChangeemail(e) {
    this.setState({
      email: e.target.value
    })
  }

  onSubmit = async (e) => {
    e.preventDefault()
    const obj = {

      name: this.state.name,
      email: this.state.email,
    };
    try{
   const res = await Axios.put('http://localhost:8000/users/' + this.state.id, obj)
   this.setState({
    id: '',
    name: '',
    email: '',
    show: false
  })
  
    this.props.history.replace('/user');
    console.log(res)
   return res
    } catch(error){
      console.log(error)
    }
  }
  handleStatus = async (e) => {
    let data_uid = e.target.getAttribute('uid')
    let st_id = e.target.getAttribute('st-id')

    const obj = {
      user_id: data_uid,
      status_id: st_id
    }
    const res = await Axios.post('http://localhost:8000/status/create', obj)
    if (st_id === 2) {
      alert('UNBANNED')
    } else {
      alert('BANNED')
    }
    console.log(res)
    console.log(obj)
  }

  render() {
    const columns = [
      {
        name: "id",
        label: "ID",
        options: {
          filter: true,
          sort: true
        }
      }
      ,{
       name: "name",
       label: "Name",
       options: {
        filter: false,
        sort: true,
       }
      },
      {
       name: "email",
       label: "Email",
       options: {
        filter: false,
        sort: false,
       }
      },
      {
        name:"user_statuses",
        label:"Status",
        options:{
          filter:false,
          sort:false,
         customBodyRender:(value,tableMeta,updateValue) => {
          if(tableMeta.rowData[3] !== undefined){

              if (tableMeta.rowData[3][0].status_id === 2) {
                return (
                  <h6 style={{ color: 'green' }}>ACTIVE</h6>
                )
              } else {
                return (
                  <h6 style={{ color: 'red' }}>BANNED</h6>
                )
              }
            }

          }
        }
      },
      {
        label:"Action",
        options:{
          filter:false,
          sort:false,
          customBodyRender: (value, tableMeta, updateValue) => {
            if (tableMeta.rowData !== undefined) {
              // console.log(tableMeta.rowData)
            }
            const data = tableMeta.rowData;
            if (data !== undefined) {
              if (data[3][0].status_id === 2) {
                return (
                  <div>
                    <Button.Group>
                      <Button primary uid={data[0]} content="Edit" onClick={this.handleShow} icon="edit"></Button>
                      <Button.Or />
                      <Button negative uid={data[0]} st-id="5" onClick={this.handleStatus}>BANNED</Button>
                    </Button.Group>
                  </div>
                )
              } else {
                return (
                  <div>
                    <Button.Group>
                      <Button primary uid={data[0]} content="Edit" onClick={this.handleShow} icon="edit"></Button>
                      <Button.Or />
                      <Button positive uid={data[0]} st-id="2" onClick={this.handleStatus}>UNBANNED</Button>
                    </Button.Group>
                  </div>
                )
              }

            }


          }
        }
      },

    ];

    //  this.state.data.map(res => {
    //    console.log (res.user_statuses[0].status_id)
    //  })
    const data = this.state.data;

    const options = {
      filterType: 'dropdown',
      responsive: 'scroll',
      selectableRows: false
    };
    return (

      <div>
        <Header />
        <Segment basic style={{ textAlign: "center", zIndex: 0 }}>
          <h2>User</h2>
          <Container>
            < MuiThemeProvider theme={this.getMuiTheme()}>
              <MUIDataTable
                title={"LIST USER"}
                data={data}
                columns={columns}
                options={options}
              />
            </MuiThemeProvider>
          </Container>
        </Segment>
        <Modal show={this.state.show} onHide={this.handleClose} size="lg" centered >
          <Modal.Header closeButton >
            <Modal.Title > Update User</Modal.Title>
          </Modal.Header>
          <Modal.Body >
            <Form onSubmit={this.onSubmit}>
                <label>Name</label>
              <Form.Field>
                <input value={this.state.name} id="name" onChange={this.onChangename} />
              </Form.Field>
                <label>Email</label>
              <Form.Field>
                <input value={this.state.email} id="email" onChange={this.onChangeemail} />
              </Form.Field>
              <Button primary >
                Save <Icon name='chevron right' />
              </Button>
            </Form>
          </Modal.Body>
        </Modal>
      </div >
    )
  }
}