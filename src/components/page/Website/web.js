
import React, { Component } from 'react';
import Header from '../../Header'
import "semantic-ui-css/semantic.min.css";
import MUIDataTable from "mui-datatables";
import { Container, Segment } from 'semantic-ui-react'
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles'
import Axios from 'axios'

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    }
  }
  componentDidMount = async () => {
    const res = await Axios.get(`http://localhost:8000/website`)
    const data = await res.data.data
    this.setState({
      data: data
    })
  }
  getMuiTheme = () => createMuiTheme({
    overrides: {
      MUIDataTableBodyCell: {
        root: {
          fontSize: 15
        }
      },
      MUIDataTableHeadCell: {
        root: {
          fontSize: 16
        }
      }
    }
  })
  render() {
    const columns = [
      {
        name: "id",
        label: "ID",
        options: {
          filter: true,
          sort: true
        }
      },
      {
        name: "domain_primary",
        label: "Domain Primary",
        options: {
          filter: true,
          sort: false,
        }
      },
      {
        name: "subdomain_default",
        label: "Subdomain Default",
        options: {
          filter: true,
          sort: false,
        }
      },
      {
        name: "name",
        label: "Name",
        options: {
          filter: true,
          sort: true,
        }
      },
      {
        name: "user",
        label: "Owner",
        options: {
          filter: false,
          sort: true,
          customBodyRender: (value, tableMeta, updateValue) => {
            if (tableMeta.rowData[4] !== undefined) {

              return tableMeta.rowData[4].name
            }
          }
        }
      },
      {
        name: "website_type",
        label: "Website",
        options: {
          filter: false,
          sort: true,
          customBodyRender: (value, tableMeta, updateValue) => {
            if (tableMeta.rowData) {

              if (tableMeta.rowData[5] === 1) {
                return 'Ecommerce'
              } else {
                return 'Company Profile'
              }
            }
          }
        }
      }
    ];

    const data = this.state.data;

    const options = {
      filterType: 'dropdown',
      responsive: 'scroll',
      selectableRows: false
    };
    return (
      <div>
        <Header />
        <Segment basic style={{ textAlign: "center", zIndex: 0 }}>
          <h2>Website</h2>
          <Container>
            < MuiThemeProvider theme={this.getMuiTheme()}>
              <MUIDataTable
                title={"LIST WEBSITE"}
                data={data}
                columns={columns}
                options={options}
              />
            </MuiThemeProvider>
          </Container>

        </Segment>
      </div >
    )
  }
}