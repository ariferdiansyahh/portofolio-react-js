import React, { Component } from 'react'
import { Card, Button, Form, Checkbox } from 'semantic-ui-react'
import Logo from '../../img/logo.png'
import Axios from 'axios'
import { instanceOf } from 'prop-types';
import { Cookies } from 'react-cookie';

class Loading extends Component {
  //other logic
    render() {
     return(
      <div class="spinner-border text-warning" style={{width: '28px', height: '28px'}} role="status">
          
      </div>   
     );
    }
 }

export default class App extends Component {
  static propTypes = {
    cookies: instanceOf(Cookies).isRequired
  };
  constructor(props) {

    super(props);
    this.onChangename = this.onChangename.bind(this);
    this.onChangepassword = this.onChangepassword.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      name: '',
      password: '',
      loading: false
    }
  }
  onChangename(e) {
    this.setState({
      name: e.target.value
    });
  }
  onChangepassword(e) {
    this.setState({
      password: e.target.value
    })
  }
  onSubmit = async (e) => {
    e.preventDefault();
    const obj = {
      name: this.state.name,
      password: this.state.password,
    };
    
    try{
      const res = await Axios.post('http://localhost:8000/login', obj)
        if (res.data.auth === true) {
          const cookies = new Cookies();
          cookies.set('admin', res.data.accessToken)
          window.location.href = '/';
        }
    }catch(error){
      
      alert('Username/Password Salah')
    }
  }
  render() {
    const cookies = new Cookies();
    var session = cookies.get('admin');
    if (session) {
      window.location.href = '/'
    }
    return (
      <div align="center">
        <div style={{ paddingTop: "45px", paddingBottom: "px" }}>
          <img
            src={Logo}
            alt="logo"
            width="310px"
            height="auto"
          />
        </div>
        <Card style={{ width: "310px", height: "360px" }}>

          <Card.Content>
            <h3 style={{ marginTop: "20px", marginBottom: "30px" }}>Otentikasi pengguna</h3>
            <Form onSubmit={this.onSubmit} >
              <Form.Field>
                <input placeholder='Username' onChange={this.onChangename} />
              </Form.Field>
              <Form.Field>
                <input type="password" placeholder='Password' onChange={this.onChangepassword} />
              </Form.Field>
              <Form.Field align="left">
                <Checkbox label='tetap masuk' />
              </Form.Field>
              {this.state.loading ? <Loading/> : ''}
              <Button color='orange' fluid size='big' style={{ marginTop: "40px" }}>Masuk</Button>
            </Form>
          </Card.Content>


        </Card>

      </div>

    )
  }
}
