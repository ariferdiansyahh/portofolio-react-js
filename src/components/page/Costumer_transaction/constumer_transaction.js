import React, { Component } from 'react';
import Header from '../../Header'
import "semantic-ui-css/semantic.min.css";
import { Modal } from 'react-bootstrap';
import {Button } from 'semantic-ui-react'
import MUIDataTable from "mui-datatables";
import { Container, Segment } from 'semantic-ui-react'
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles'
import Axios from 'axios'
import { Grid } from 'semantic-ui-react'

export default class App extends Component {
  constructor(props) {
    super(props);

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleStatus = this.handleStatus.bind(this);
    this.onChangename = this.onChangename.bind(this);
    this.onChangeemail = this.onChangeemail.bind(this);

    this.state = {
      data: [],
      data_transaction: [],
      transaction: [],
      name: '',
      email: '',
      show: false,
    }
  }
  componentDidMount = async () => {
    const res = await Axios.get(`http://localhost:8000/customer-transactions`)
    const data = await res.data.data
    console.log(data)
    this.setState({
      data: data
    })
  }

  getMuiTheme = () => createMuiTheme({
    overrides: {
      MUIDataTableBodyCell: {
        root: {
          fontSize: 15
        }
      },
      MUIDataTableHeadCell: {
        root: {
          fontSize: 16
        }
      }
    }
  })

  onClickEdit(e) {
    console.log('Edit')
  }
  onClickAction(e) {
    console.log('Action')
  }

  handleClose() {
    this.setState({
      data_transaction: '',
      transaction: '',
      show: false
    });
  }

  handleShow = async (e) => {

    let data_uid = await e.target.getAttribute('uid')
    console.log(data_uid)
    if (data_uid !== null) {
      const res = await Axios.get('http://localhost:8000/customer-transactions/' + data_uid)
      const data = await res.data.data
      const transactions = await res.data.data.transaction
      console.log(data)
      this.setState({
        data_transaction: data,
        transaction:transactions,
        show: true
      })
    }


  }
  onChangename(e) {
    this.setState({
      name: e.target.value
    });
  }
  onChangeemail(e) {
    this.setState({
      email: e.target.value
    })
  }

  onSubmit = (e) => {
    e.preventDefault()
    const obj = {

      name: this.state.name,
      email: this.state.email,
    };
    Axios.put('http://localhost:8000/customer-transactions/' + this.state.id, obj)
      .then(res => {
        this.setState({
          id: '',
          name: '',
          email: '',
          show: false
        })
        console.log(res)
      })
  }
  handleStatus = async (e) => {
    let data_uid = e.target.getAttribute('uid')
    let st_id = e.target.getAttribute('st-id')

    const obj = {
      user_id: data_uid,
      status_id: st_id
    }
    const res = await Axios.post('http://localhost:8000/status/create', obj)
    if (st_id === 2) {
      alert('UNBANNED')
    } else {
      alert('BANNED')
    }
    console.log(res)
    console.log(obj)
  }

  render() {
    const columns = [
      {
        name: "id",
        label: "ID",
        options: {
          filter: true,
          sort: false
        }
      }
      ,
      {
        name: "first_name",
        label: "Full Name",
        options: {
          filter: false,
          sort: true,
          customBodyRender: (value, tableMeta, updateValue) => {
            if (tableMeta.rowData !== undefined) {
               console.log(tableMeta.rowData)
                const firstName = tableMeta.rowData[1]
                const middleName = tableMeta.rowData[2]
                const lastName = tableMeta.rowData[3]
               const fullName = firstName + ' ' + middleName + ' ' + lastName
               return fullName
            }
        }
      }
      },
      {
        name: "middle_name",
        options: {
          display: false,
          sort: false,
          filter: false
        }
      },
      {
        name: "last_name",
        options: {
          display: false,
          sort: false,
          filter: false
        }
      },
      {
        name: "email_address",
        label: "Email",
        options: {
          filter: false,
          sort: false,
        }
      },
      {
        name: "phone_number",
        label: "Phone Number",
        options: {
          filter: false,
          sort: false,
        }
      },
      {
        name: "transaction_id",
        label: "ID Merchant",
        options: {
          filter: false,
          sort: false,
        }
      },
      {
        name: "Action",
        options: {
          filter: false,
          sort: false,
          customBodyRender: (value, tableMeta, updateValue) => {
            if (tableMeta.rowData !== undefined) {
               //console.log(tableMeta.rowData)
            }
            const data = tableMeta.rowData;
            if (data !== undefined) {
              if (data) {
                return (
                  <div>
                    <Button.Group>
                      <Button primary uid={data[0]} content="Detail" onClick={this.handleShow} icon="clipboard outline"></Button>
                    </Button.Group>
                  </div>
                )
              }

            }


          }
        }
      },

    ];
    const data = this.state.data;
    //  this.state.data.map(res => {
    //    console.log (res.user_statuses[0].status_id)
    //  })
    

    const options = {
      filterType: 'dropdown',
      responsive: 'scroll',
      selectableRows: false
    };
    return (

      <div>
        <Header />
        <Segment basic style={{ textAlign: "center", zIndex: 0 }}>
          <h2>Costumer Transaction</h2>
          <Container>
            < MuiThemeProvider theme={this.getMuiTheme()}>
              <MUIDataTable
                title={"LIST CUSTOMER TRANSACTION"}
                data={data}
                columns={columns}
                options={options}
              />
            </MuiThemeProvider>
          </Container>
        </Segment>
        <Modal show={this.state.show} onHide={this.handleClose} size="lg" centered >
          <Modal.Header closeButton >
            <Modal.Title > Detail Customer Transaction</Modal.Title>
          </Modal.Header>
          <Modal.Body >
            <Container>
                <Grid divided='vertically'>
                  <Grid.Row columns={2}>
                    <Grid.Column width={5}>
                      <p> Name</p>
                    </Grid.Column>
                    <Grid.Column width={7}>
                      <h6> : &nbsp; {this.state.data_transaction.first_name + ' ' + this.state.data_transaction.middle_name + ' ' + this.state.data_transaction.last_name}</h6>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      <p> Email</p>
                    </Grid.Column>
                    <Grid.Column width={7}>
                      <h6> : &nbsp; {this.state.data_transaction.email_address}</h6>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      <p> Transaction ID Merchant</p>
                    </Grid.Column>
                    <Grid.Column width={7}>
                      <h6> : &nbsp; {this.state.data_transaction.transaction_id}</h6>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      <p> Address</p>
                    </Grid.Column>
                    <Grid.Column width={7}>
                      <h6> : &nbsp; {this.state.data_transaction.address}</h6>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      <p> Phone Number</p>
                    </Grid.Column>
                    <Grid.Column width={7}>
                      <h6> : &nbsp; {this.state.data_transaction.phone_number}</h6>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      <p> Shipping Method</p>
                    </Grid.Column>
                    <Grid.Column width={7}>
                      <h6> : &nbsp; {this.state.data_transaction.shipping_method}</h6>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      <p> Ordered Item ID</p>
                    </Grid.Column>
                    <Grid.Column width={7}>
                      <h6> : &nbsp; {this.state.data_transaction.ordered_item_id}</h6>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      <p> Transaction Status</p>
                    </Grid.Column>
                    <Grid.Column width={7}>
                      <h6> : &nbsp; {this.state.data_transaction.trxstatus}</h6>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      <p> Payment Date Time</p>
                    </Grid.Column>
                    <Grid.Column width={7}>
                      <h6> : &nbsp; {this.state.transaction.payment_date_time}</h6>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      <p> Payment Code</p>
                    </Grid.Column>
                    <Grid.Column width={7}>
                      <h6> : &nbsp; {this.state.transaction.paymentcode}</h6>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      <p> Approval Code</p>
                    </Grid.Column>
                    <Grid.Column width={7}>
                      <h6> : &nbsp; {this.state.transaction.approvalcode}</h6>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      <p> Total Amount</p>
                    </Grid.Column>
                    <Grid.Column width={7}>
                      <h6> : &nbsp; {this.state.transaction.totalamount}</h6>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
            </Container>
          </Modal.Body>
        </Modal>
      </div >
    )
  }
}