import React from 'react'
import { Dropdown, DropdownHeader, Icon, DropdownDivider } from 'semantic-ui-react'

import { Link } from "react-router-dom"
import Isinotif from './Isinotif'


const DropdownExampleHeader = () => (
    <Dropdown icon='bell outline large ' className='icon' >
        <Dropdown.Menu style={{ maxWidth: 50 }}>
            <DropdownHeader align="center"><h4>Notification</h4></DropdownHeader>

            <Isinotif />

            <DropdownDivider />
            <Dropdown.Header align="center">  <Link to="/notif" style={{ color: "black" }}><Icon name="eye" />see all</Link></Dropdown.Header>
        </Dropdown.Menu>
    </Dropdown>
)

export default DropdownExampleHeader