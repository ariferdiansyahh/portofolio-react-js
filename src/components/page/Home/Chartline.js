import React, { Component } from 'react'
import { Line } from 'react-chartjs-2';
import { Card } from 'semantic-ui-react'


class chart extends Component {
    constructor(props) {
        super(props)
        this.state = {
            chartData: {
                labels: ['a', 'b', 'c', 'd', 'e'],
                datasets: [{
                    label: 'populasi',
                    data: [
                        12313,
                        12312,
                        13132,
                        13132,
                        12444
                    ],
                    backgroundColor: [
                        '#fcdfd1',
                        '#ffd6c4',
                        '#ffa37a',
                        '#f76625',
                        '#c94408',
                        '#6d2504'
                    ]
                }]
            }
        }
    }

    render() {
        return (
            <div className="chart">
                <Card style={{ width: "100%" }} className="shadow">
                    < Line
                        data={this.state.chartData}
                        options={{}}
                    />
                </Card>
            </div>
        )
    }
}

export default chart
