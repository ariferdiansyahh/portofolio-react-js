import React from 'react'
import Header from '../../Header'
import Card1 from './Card1'
import Card2 from './Card2'
import Card3 from './Card3'
import Chart0 from './Chart0'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Grid, Container, Segment } from 'semantic-ui-react'


class Home extends React.Component {

    render() {
        return (

            <div>
                <Header />
                <Segment basic>

                    <Container>
                        <Grid divided='vertically'>
                            <Grid.Row columns={3} Align="center">
                                <Grid.Column >
                                    
                                    <Card1 />
                                    
                                </Grid.Column>
                                <Grid.Column>
                                        
                                        <Card2 />
                                    
                                </Grid.Column>
                                <Grid.Column>

                                        <Card3 />
                                    
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>

                        <Chart0 />


                    </Container>

                </Segment>

            </div >
        )
    }
}

export default Home