import React from 'react'
import { Grid } from 'semantic-ui-react'
import Chartuser from './Chartuser'
import Chartwebsite from './Chartwebsite'
import Chartline from './Chartline'
import Chartbar from './Chartbar'

const GridExampleVerticallyDivided = () => (
    <Grid  >
        <Grid.Row columns={2} Align="center">

            <Grid.Column >
                <h2>User</h2>
                <Chartuser />
            </Grid.Column>

            <Grid.Column>
                <h2>Website</h2>
                <Chartwebsite />
            </Grid.Column>

        </Grid.Row>
        <Grid.Row columns={2} Align="center">

            <Grid.Column >
                <Chartline />
            </Grid.Column>

            <Grid.Column>
                <Chartbar />
            </Grid.Column>

        </Grid.Row>
    </Grid>
)

export default GridExampleVerticallyDivided
