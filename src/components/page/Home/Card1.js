import React, { Component } from 'react'
import { Card, Icon } from 'semantic-ui-react'
import Axios from 'axios'

class Circle extends Component {
    render (){
        return(
            <div className="spinner-border text-light" style={{width: '24px', height: '24px'}} role="status">
            
            </div>
        )
    }
}
class Loading extends Component {
    //other logic
      render() {
       return(
        <div className="spinner-border text-light" style={{width: '24px', height: '24px'}} role="status">
            
        </div> 
       );
      }
   }

export default class App extends Component{
    constructor(props) {
        super(props);
        this.state = {
        data:[],
        loading: false
        }
    }
    componentDidMount = async() => {
        try{
        const res = await Axios.get('http://localhost:8000/balance')
        const data = await res.data
        this.setState({
            data:data,
            loading: true                                                                                                                                      
        })
        const rupiah = this.state.data.values;
        const number = parseInt(rupiah.replace(/[^0-9]/g, ''), 10);
        if (number >= 200000000){
            document.getElementById('circle').innerHTML='<div style="height: 20px; width: 20px; background-color: green; border-radius: 30px; float: right; margin-top: 3px; margin-right: 50px; display: inline;"></div>'
            console.log('hijau')
        } else if (number < 200000000){
            document.getElementById('circle').innerHTML='<div style="height: 20px; width: 20px; background-color: orange; border-radius: 30px; float: right; margin-top: 3px; margin-right: 50px; display: inline;"></div>'
            console.log('kuning')
        } else if (number < 100000000) {
            document.getElementById('circle').innerHTML='<div style="height: 20px; width: 20px; background-color: red; border-radius: 30px; float: right; margin-top: 3px; margin-right: 50px; display: inline;"></div>'
            console.log('merah')
        } else{
            console.log('ga ada')
        }
        console.log(number)           
    } catch(err){
        console.log(err)
    }
}

    render()
    {
        return(
        <Card style={{ backgroundColor: '#c6231d', width: "100%" }} className="shadow ">
        <Card.Content textAlign='center' style={{ paddingTop: 30, paddingBottom: 20 }}>
        
            <Card.Header style={{ color: "white", paddingBottom: 15}}>
                <Icon name="cart" size="big"/>
            </Card.Header>
        
            <Card.Meta style={{ color: "white" }}>
                <h3>Domain Reseller Balance</h3>
            </Card.Meta>
        
            <Card.Description style={{ color: "white" }}>
            
                {this.state.loading ?<div><h4 style={{marginLeft: '60px', display: 'inline'}}>{this.state.data.values}<span id="circle"></span></h4> </div> : <Loading/>} 
            </Card.Description>
        
        </Card.Content>
        </Card>
        )
    }
}