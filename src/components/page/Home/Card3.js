import React, { Component } from 'react'
import { Card, Icon } from 'semantic-ui-react'
import Axios from 'axios'

class Loading extends Component {
    //other logic
      render() {
       return(
        <div className="spinner-border text-light" style={{width: '24px', height: '24px'}} role="status">
            
        </div>  
       );
      }
   }

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
          data:[],
          loading: false
        }
    }
    componentDidMount = async() => {
        try{
        const res = await Axios.get(`http://localhost:8000/website`)
        const data = await res.data
        this.setState({
            data:data,
            loading: true
        })  
    } catch(err){
        console.log(err)
    }  
    }
    render()
    {
        return(
        <Card style={{ backgroundColor: '#149e1b', width: "100%" }} className="shadow">

        <Card.Content textAlign='center' style={{ paddingTop: 30, paddingBottom: 20 }} >

            <Card.Header style={{ color: "white", paddingBottom: 15 }}>
                <Icon name='world' size="big" />
            </Card.Header>

            <Card.Meta style={{ color: "white" }}>
                <h3>Website</h3>
            </Card.Meta>

            <Card.Description style={{ color: "white" }}>
                { this.state.loading ? <h4>{new Intl.NumberFormat('en-IN',{ maximumSignificantDigits: 3 }).format(this.state.data.jumlah)}</h4> : <Loading/>}
            </Card.Description>

        </Card.Content>
        </Card>
        )
    }
}

