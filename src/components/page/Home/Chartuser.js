import React, { Component } from 'react'
import { Pie } from 'react-chartjs-2';
import { Card } from 'semantic-ui-react'
import Axios from 'axios'

class Loading extends Component {
    //other logic
      render() {
       return(
        <div className="spinner-border text-warning m-5" style={{width: '13rem' , height: '13rem'}} role="status">
            <span className="sr-only"></span>
        </div>  
       );
      }
   }

export default class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data:[],
            loading: false
        }

    }
    
    componentDidMount= async() => {
        try{
        const res = await Axios.get(`http://localhost:8000/status`)
            const persons = await res.data;
            const data = await [persons.active, persons.bann];
            this.setState({
                loading: true,
                chart: {
                    labels: ['ACTIVE', 'BANNED'],
                    datasets: [{
                        data: data,
                        backgroundColor: ['#FFAC54', '#E67700'],
                        borderWidth: 1
                    }]
                
                },
                options: {
                    legend: {
                        labels: {
                            fontColor: 'black',
                        }

                    },
                    tooltips: {
                        callbacks: {
                            afterLabel: function(tooltipItem, data) {
                            //get the concerned dataset
                            var dataset = data.datasets[tooltipItem.datasetIndex];
                            //calculate the total of this data set
                            var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                                return previousValue + currentValue;
                            });
                            //get the current items value
                            var currentValue = dataset.data[tooltipItem.index];
                            //calculate the precentage based on the total and current item, also this does a rough rounding to give a whole number
                            var percentage = Math.floor(((currentValue/total) * 100)+0.5);

                            return 'PERCENTAGE : ' +  percentage+'%';
                            }
                        }
                    }
                }
            })
        }catch(err){
            console.log(err)
        }
    }
    chart(){
        return  < Pie
        data={this.state.chart}
        options={this.state.options}
    />
    }

    render() {
        return (
            <div className="chart shadow">
            {this.state.loading ?
                <Card style={{ width: "100%" }}>
                    {this.chart()} 
                </Card>
            : <Loading/>}
            </div>
        )
    }
}

