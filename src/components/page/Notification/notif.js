import React from 'react'
import Menu from './Menunotif'
import Modal from './Modal3'
import Header from '../../Header'
import "semantic-ui-css/semantic.min.css";
import { Container } from 'semantic-ui-react'
const Contact = () => (
    <div>
        <Header />

        <Container style={{ paddingTop: 40, paddingBottom: 30 }}>
            <div style={{ textAlign: "right " }}>
                < Modal />
            </div>
            <Menu />
        </Container>

    </div >
)

export default Contact