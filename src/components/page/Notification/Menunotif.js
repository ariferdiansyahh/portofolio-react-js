import React from 'react'
import { Item } from 'semantic-ui-react'
import Logo from './img/logokotak.png'


const ItemExampleItems = () => (
    <Item.Group style={{ marginTop: "-35px" }}>
        <div style={{ paddingBottom: "10px" }}>
            <h3>NOTIFICATION</h3>
        </div>

        <Item >
            <img src={Logo} alt=".." height="75" weight="75" className="rounded border" />
            <Item.Content style={{ marginLeft: "15px" }}>
                <Item.Header><h4>Name</h4></Item.Header>
                <Item.Meta style={{ marginTop: "-3px" }}><h5>Lorem Ipsum adalah title</h5></Item.Meta>
                <Item.Description style={{ marginTop: "-7px" }}>
                    <p>
                        Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks....
                    </p>
                </Item.Description>
                <Item.Extra style={{ marginTop: "-7px" }}><h6>22-03-19</h6></Item.Extra>
            </Item.Content>
        </Item>
        <hr></hr>
        <Item >
            <img src={Logo} alt=".." height="75" weight="75" className="rounded border" />
            <Item.Content style={{ marginLeft: "15px" }}>
                <Item.Header><h4>Name</h4></Item.Header>
                <Item.Meta style={{ marginTop: "-3px" }}><h5>Lorem Ipsum adalah title</h5></Item.Meta>
                <Item.Description style={{ marginTop: "-7px" }}>
                    <p>
                        Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks....
                    </p>
                </Item.Description>
                <Item.Extra style={{ marginTop: "-7px" }}><h6>22-03-19</h6></Item.Extra>
            </Item.Content>
        </Item>
        <hr></hr>
        <Item >
            <img src={Logo} alt=".." height="75" weight="75" className="rounded border" />
            <Item.Content style={{ marginLeft: "15px" }}>
                <Item.Header><h4>Name</h4></Item.Header>
                <Item.Meta style={{ marginTop: "-3px" }}><h5>Lorem Ipsum adalah title</h5></Item.Meta>
                <Item.Description style={{ marginTop: "-7px" }}>
                    <p>
                        Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks....
                    </p>
                </Item.Description>
                <Item.Extra style={{ marginTop: "-7px" }}><h6>22-03-19</h6></Item.Extra>
            </Item.Content>
        </Item>
        <hr></hr>
        <Item >
            <img src={Logo} alt=".." height="75" weight="75" className="rounded border" />
            <Item.Content style={{ marginLeft: "15px" }}>
                <Item.Header><h4>Name</h4></Item.Header>
                <Item.Meta style={{ marginTop: "-3px" }}><h5>Lorem Ipsum adalah title</h5></Item.Meta>
                <Item.Description style={{ marginTop: "-7px" }}>
                    <p>
                        Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks....
                    </p>
                </Item.Description>
                <Item.Extra style={{ marginTop: "-7px" }}><h6>22-03-19</h6></Item.Extra>
            </Item.Content>
        </Item>
        <hr></hr>
       
    </Item.Group>
)

export default ItemExampleItems