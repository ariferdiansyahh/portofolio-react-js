import React from 'react'
import { Modal } from 'react-bootstrap';
import { Icon, Button } from 'semantic-ui-react'
import Form from './Form'


class Example extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);

        this.state = {
            show: false,
        };
    }

    handleClose() {
        this.setState({ show: false });
    }

    handleShow() {
        this.setState({ show: true });
    }

    render() {
        return (
            <>

                <Button size='medium' inverted color='orange' content='Add Notification' onClick={this.handleShow} />

                <Modal show={this.state.show} onHide={this.handleClose} size="lg" >

                    <Modal.Header closeButton >
                        <Modal.Title > Notification</Modal.Title>
                    </Modal.Header>


                    <Modal.Body >
                        <Form />
                    </Modal.Body>


                    <Modal.Footer>
                        <Button primary>
                            Proceed <Icon name='chevron right' onClick={this.handleClose} />
                        </Button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    }
}

export default Example