
import React, { Component } from 'react';
import { Form, TextArea } from 'semantic-ui-react'
import Editor from './CKeditor'
import Select from 'react-select';

class Selectoption extends React.Component {
    constructor(props) {
        super(props);
    
        this.handleSpecific = this.handleSpecific.bind(this);
    
        this.state = {
        selectedOption: null,
        }
  
    }
    handleSpecific= (selectedOption) => {
        this.setState({ selectedOption });
        console.log(`Option selected:`, selectedOption);
      }
    render() {
        const options = [
            { value: 'chocolate', label: 'Chocolate' },
            { value: 'strawberry', label: 'Strawberry' },
            { value: 'vanilla', label: 'Vanilla' }
          ];
        const { selectedOption } = this.state;
      return (
          <div>
          <label>To Specific</label>
      <Select
      value={selectedOption}
      onChange={this.handleSpecific}
      options={options}
      placeholder="Select Recipient"
        />
    </div>
    )
    }
  }
  

export default class App extends Component{
    constructor(props) {
        super(props);
    
        this.handleChange = this.handleChange.bind(this);
    
        this.state = {
        selectedOption: null,
        show: false,
        showSelect:false
        }
    }


    handleChange = async(e, data) => {
        const { value } = data;
        
        if(value === "Specific"){
            console.log('sukses')
            this.setState({ showSelect: true })
        }else{
            console.log('gagal')
            this.setState({ showSelect: false })
            
        }
        console.log(value)
    }
    render(){
        
        const options = [
            { key: 'a', text: 'All', value: 'All' },
            { key: 'S', text: 'Specific', value: 'Specific' },
        ]
        return(
            <div>
            <Form>
        <label>To</label>
        <Form.Field>
        <Form.Select options={options} id="foo" placeholder="Select" onChange={this.handleChange} />
        </Form.Field>
        <Form.Field >
        {this.state.showSelect ? <Selectoption /> : ''}
        </Form.Field>
        <label>Title</label>
        <Form.Field>
            <input placeholder="Title"/>
        </Form.Field>

        <label>Ringkasan</label>
        <Form.Field
            id='form-textarea-control-opinion'
            control={TextArea}
            placeholder="Ringkasan"
        />
        
        <label>Content</label>
            <Editor />
       

    </Form>
    </div>
        )
    }
}